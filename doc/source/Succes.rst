Les succes du jeu:
==================

.. _succes-major:
Majorer son année
-----------------
    0 fun cette année dit donc

.. _succes-ninor:
Minorer son année
-----------------
    Branleur

Dormeur
-------
    S'endormir à un cours.

La belle au bois dormant
------------------------
    Dormir à tous les cours.

La pile electrique
------------------
    Finir l'année sans dormir en cours.