.. Etablissement de Niveau Supreme Simulateur documentation master file, created by
   sphinx-quickstart on Fri Dec 30 17:12:17 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Etablissement de Niveau Supreme Simulateur's documentation!
======================================================================

.. toctree::
   personnages/index.rst
   lieux/index.rst
   mecanisme/index.rst
   quest/index.rst
   Succes.rst

Presentation
============


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
