===============
Les personnages
===============


.. toctree::
    :titlesonly:
    :glob:

    *

Love interest
=====================

    + :doc:`Noemi_Pietra`
    + :doc:`Frederic_LHotelier`
    + :doc:`Momo`
    + :doc:`Louis-Etienne_Pitou`

Enemis
======================

    + :doc:`Fabien_Scosino`