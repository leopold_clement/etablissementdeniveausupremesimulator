# EtablissementDeNiveauSupremeSimulator


## Présentation
Ce dépot contient une visual novel sur l'ENS Paris-Saclay

## Utilisation
Pour copier le dépot sur votre machine, il suffit de taper :
``` bash
git clone https://gitlab.com/leopold_clement/etablissementdeniveausupremesimulator.git
````
Il faut se positioner dans le dossier des projet de Renpy si vous voulez pouvoir exploiter le jeu.

### Génération de la documentation
La documentation comporte tout le _lore_ du jeu.
``` bash
cd doc
pip install -r requirements.txt
make html
```
Le point d'entrée du site générer est `doc/build/html/index.html`.

### Génération de l'executable du jeu et version de dévelopement
Le projet est utilisable grace à [RenPy](https://renpy.org), un moteur de jeu dédié au visual novel.
Pour générer un executable ou jouer à la version de dévelopement, il faut suivre la procedure d'instalation de renpy, et clonner le dépos dans le dossier des projets de Renpy