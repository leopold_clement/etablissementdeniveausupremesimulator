﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define e = Character("Eileen")
define joueur = Character("Moi")

# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene bg room

    e "c'est parti mon kiki"

    joueur "A moi de jouer"

    "J'adore l'info"

    menu bite:
        e "Je flétrie face au vent et sent le parmesant, qui suis-je ?"
        "Un phare breton":
            e "Perdu, c'est ma bite"
            joueur "Subtile comme blague"
        "Ma bite":
            e "bien jouer, tu es un connaisseur"
            joueur "oui"

    return
